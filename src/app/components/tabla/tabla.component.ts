import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { Usuario } from 'src/app/sharen/usuario';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.scss']
})
export class TablaComponent implements OnInit {
  usuario:Usuario[]
  constructor( private servis:ApiService) { 
    this.usuario=[{}]
  }

  async ngOnInit() {
    this.usuario= await this.servis.getAll()
    
  }

}
