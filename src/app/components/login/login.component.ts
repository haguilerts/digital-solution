import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { Usuario } from 'src/app/sharen/usuario';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  usurioCorrecto: boolean = false
  formLogin: FormGroup
  constructor(private server: ApiService, private ruta: Router) {
    this.formLogin = new FormGroup({
      user: new FormControl('', [Validators.required, ]),
      pass: new FormControl('', [Validators.required, ]),
    })
  }

  ngOnInit(): void {
  }
  async enviar() {
    if (this.formLogin.invalid) {
      this.reset()
    } else {
      let api = await this.server.getAll()
      this.ruta.navigate([''])
      this.usurioCorrecto = this.getUsusario(api, this.formLogin.value.user, this.formLogin.value.pass)
     
      if (!this.usurioCorrecto) {
        this.malIngresado()
        
        this.reset()
      }
    }

  }
  private reset() {
    this.formLogin.reset()
  }
  private getUsusario(api: Usuario[], user: Usuario, pass: Usuario): boolean {

    let res = false
    api.forEach(obj => {
     
      if (obj.name == user && obj.dni == pass) {
       
        res = true
        if (user == 'Lucas' && pass == 56214758) {
          this.bienvenido(user)
          this.ruta.navigate(['/admin'])
        } else {
          
          this.bienvenido(user)
        }
      }
    });
    return res
  }


  private bienvenido(name?:any) {
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: `Bienvenido ${name} 😊👍`,
      showConfirmButton: false,
      timer: 2000
    })
    .then((result) => {
      if(name == 'Lucas'){
        console.log('lucas',this.ruta.navigate(['/admin']) )
        this.ruta.navigate(['/admin'])
      }
      this.ruta.navigate(['inicio'])
    })
    
  }
  private malIngresado() {
    Swal.fire({
      position: 'center',
      icon: 'error',
      title: 'Mal ingresado 😞👎!! <br> vuelta a intenralo ',
      showConfirmButton: false,
      timer: 2000
    })
    .then((result) => {
      this.ruta.navigate(['/'])
    })
    
  }
  ngOnDestroy() {
    this.reset()
  }
 
}
