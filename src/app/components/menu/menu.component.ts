import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  menu:string='';
  constructor() { }

  ngOnInit(): void {
    this.menu='menu-close'
  }
  menuOpenClose(menu:string){
    if(menu=='menu-open'){
      this.menu='menu-open'
    }else{
      this.menu='menu-close'
    }
  }
  onclick(){
    
  }
}
