export interface Usuario{
    id?:number,
    name?:string,
    age?:number,
    dni?:number,
    phone?:number,
    location?:string
}