import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { TablaComponent } from './components/tabla/tabla.component';
import { VideoComponent } from './components/video/video.component';

const routes: Routes = [
  {path:'', pathMatch:'full',redirectTo:'/'},
  {path:'', component: LoginComponent},
  {path:'inicio', component: VideoComponent},
  {path:'admin', component: TablaComponent},  
  { path: '**', redirectTo: '/', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    initialNavigation: 'enabled',
    }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
