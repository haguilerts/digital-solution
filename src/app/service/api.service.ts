import { Injectable } from '@angular/core';
import { Usuario } from '../sharen/usuario';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { apiClient } from '../sharen/apiCliente';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private usuario:Usuario[];
  constructor(private httpClient:HttpClient) { 
    this.usuario=apiClient
  }
  getAll():Promise<Usuario[]>{
    return new Promise((resolve,rejects)=>{
      resolve(this.usuario)
    })
  }
 
}
